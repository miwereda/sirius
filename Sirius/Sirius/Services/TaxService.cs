﻿using Sirius.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sirius.Services
{
    public class TaxService: ITaxService
    {
        public double GetTaxRate()
        {
            return 0.7;
        }
    }
}