﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Interfaces
{
    public interface ITaxService
    {
        double GetTaxRate();
    }
}
